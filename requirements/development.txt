-r production.txt

bandit>=1.1.0
bumpversion>=0.5.3
colorama>=0.3.7
coverage>=4.2
coveralls>=1.1
invoke>=0.13.0
pytest>=2.9.2
pytest-cov>=2.3.1
PyYAML>=3.11
Sphinx>=1.4.5
tox>=2.3.1
watchdog>=0.8.3
